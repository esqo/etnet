package com.etnetera.hr.data;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Column(nullable = false, length = 30)
	private String name;
	
	@ElementCollection
	@Column(nullable = false, length = 10)
	private List<String> version;
	
	@Column(nullable = true, length = 20)
	private Date deprecationDate;
	
	@Column(nullable = true, length = 10)
	private Float hypeLevel;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name, List<String> version) {
		this.name = name;
		this.version = version;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getVersion() {
		return version;
	}

	public void setVersion(List<String> version) {
		this.version = version;
	}

	public Date getDeprecationDate() {
		return deprecationDate;
	}

	public void setDeprecationDate(Date deprecationDate) {
		this.deprecationDate = deprecationDate;
	}

	public Float getHypeLevel() {
		return hypeLevel;
	}

	public void setHypeLevel(Float hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + ", name=" + name + ", version=" + version + ", deprecationDate="
	+ deprecationDate + ", hypeLevel(whatever it means :D)=" + hypeLevel + "]";
	}
	

}
