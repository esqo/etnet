package com.etnetera.hr.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.response.Constants;
import com.etnetera.hr.response.Response;
import com.etnetera.hr.response.Status;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
@RequestMapping("/frameworks")
public class JavaScriptFrameworkController {

	
	private final JavaScriptFrameworkRepository repository;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
		this.repository = repository;
	}
	
	// Could use ResponseEntity for the responses
	// add authetication for the rest all
	// @RolesAllowed("AUTH")
	@GetMapping(value = "/list", produces = "application/json")
	public Iterable<JavaScriptFramework> frameworks() {
		return repository.findAll();
	}
	
	@GetMapping(value = "/get/byid/{id}", produces = "application/json")
	public JavaScriptFramework getFrameworkById(@PathVariable("id") Long id ) {
		JavaScriptFramework fw =repository.findById(id).get();
		return repository.findById(id).get();
	}
	
	@GetMapping(value = "/get/byname/{name}", produces = "application/json")
	public JavaScriptFramework getFrameworkByName(@PathVariable("name") String name ) {
		return repository.findByName(name);
	}
	
	
	
	@PatchMapping(value = "/update", produces = "application/json")
	public Response updateFramework(@RequestBody JavaScriptFramework framework) {
		Response response = new Response();
		
		if(framework.getId() == null) {
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.missingFramework.replaceAll("#1", " id "));
			return response;
		}else {
			JavaScriptFramework fw = repository.findById(framework.getId()).get();
			
			response = updateFrameworkWithNewOne(fw, framework, response);
			return response;
		}
	}
	
	// to implement delete version we would need another method for the rest api
	// here we just append if new elements arrived
	
	private Response updateFrameworkWithNewOne(JavaScriptFramework existing, JavaScriptFramework updated, Response response) {
		boolean existingWasUpdated = false;
		if(!(updated.getName().equals("")) && !existing.getName().equals(updated.getName())) {
			existing.setName(updated.getName());
			existingWasUpdated = true;
		}
		if(updated.getHypeLevel() != null && existing.getHypeLevel() != (updated.getHypeLevel())) {
			existing.setHypeLevel(updated.getHypeLevel());
			existingWasUpdated = true;
		}
		if(updated.getDeprecationDate() != null) {
			if(existing.getDeprecationDate() == null) {
				existing.setDeprecationDate(updated.getDeprecationDate());
				existingWasUpdated = true;
			}else {
				if(existing.getDeprecationDate().compareTo(updated.getDeprecationDate()) < 0) {
					existing.setDeprecationDate(updated.getDeprecationDate());
					existingWasUpdated = true;
				}
			}
		}
		if(updated.getVersion() != null && updated.getVersion().size() > 0) {
			List<String> existingVersions = existing.getVersion();
			List<String> newVersions = updated.getVersion();
			
			for(String version: newVersions) {
				if(!existingVersions.contains(version)) {
					existingVersions.add(version);
					existingWasUpdated = true;
				}
			}
		}
		if(existingWasUpdated) {
			repository.save(existing);
			response.setStatus(Status.OK);
			response.setId(existing.getId());
		}else {
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.updateFrameworkError);
		}
		
		return response;
	}
	
	@PutMapping(value = "/create", consumes = "application/json", produces = "application/json")
	public Response createFramework(@RequestBody JavaScriptFramework framework) {
		Response response = checkForRequiredFieldsOnCreate(framework);
		if(response != null) {
			return response;
		}else {
			response = new Response();
			JavaScriptFramework fw = repository.save(framework);
			response.setStatus(Status.OK);
			response.setId(fw.getId());
			return response;
		}
	}
	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public Response deleteFramework(@PathVariable("id") Long id ) {
		Response response = new Response();
		try {
			if(id == null) {
				response.setErrorReason(Constants.deleteIdMissing);
				response.setStatus(Status.ERROR);
				return response;
			}
			JavaScriptFramework fw = repository.findById(id).get();
			if(fw != null) {
				response.setStatus(Status.OK);
				response.setId(id);
				repository.deleteById(id);
			}else {
				response.setStatus(Status.OK);
				response.setErrorReason(Constants.frameworkNotFound.replaceAll("#1", Long.toString(id)));
			}
			return response;
		} catch (Exception e) {
			response.setErrorReason(e.toString());
			response.setStatus(Status.ERROR);
			return response;
		}
		
	}
	
	private Response checkForRequiredFieldsOnCreate(JavaScriptFramework framework) {
		Response response = null;
		
		if(framework == null) {
			response = new Response();
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.missingFramework.replaceAll("#1", " "));
			return response;
		}
		if(framework.getName().equals("")) {
			response = new Response();
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.missingFramework.replaceAll("#1", " name "));
			return response;
		}
		if(framework.getVersion() == null) {
			response = new Response();
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.missingFramework.replaceAll("#1", " version "));
			return response;
		}
		if(framework.getVersion().size() == 0) {
			response = new Response();
			response.setStatus(Status.ERROR);
			response.setErrorReason(Constants.missingFramework.replaceAll("#1", " version "));
			return response;
		}
		
		return response;
	}

}
