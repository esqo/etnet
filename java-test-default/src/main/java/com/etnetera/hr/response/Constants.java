package com.etnetera.hr.response;

public class Constants {

	public static String missingFramework = "Missing framework#1from request body.";

	public static String updateFrameworkError = "Framework was not updated, there was no new elements in json body.";
	public static String deleteIdMissing = "Missing id from delete request.";
	public static String frameworkNotFound = "Framework with id #1 not found.";
}
