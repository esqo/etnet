package com.etnetera.hr.response;

public class Response {

	private String errorReason;
	private Status status;
	private Long id;

	public Response() {

	}

	public Response(String errorReason, Status status, Long id) {
		super();
		this.errorReason = errorReason;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getErrorReason() {
		return errorReason;
	}

	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
