package com.etnetera.hr.repository;

import java.sql.Date;

import org.springframework.data.repository.CrudRepository;

import com.etnetera.hr.data.JavaScriptFramework;

/**
 * Spring data repository interface used for accessing the data in database.
 * 
 * @author Etnetera
 *
 */
public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFramework, Long> {

	JavaScriptFramework findByName(String frameworkName);
	
	// prepared for listing deprecation dates < than allowed value
	Iterable<JavaScriptFramework> findByDeprecationDate(Date date);
	
	Iterable<JavaScriptFramework> findByHypeLevel(Float hype);
	
}
