package com.etnetera.hr;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.etnetera.hr.controller.JavaScriptFrameworkController;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavaScriptFrameworkTests {

	@Autowired
	private MockMvc mvc;
	
	private String createPath = "/frameworks/create";
	private String updatePath = "/frameworks/update";
	private String deletePath = "/frameworks/delete/";
	private String getByIdPath = "/frameworks/get/byid/";
	private String getByNamePath = "/frameworks/get/byname/";

	@Autowired
	private JavaScriptFrameworkRepository repository;

	@InjectMocks
	private JavaScriptFrameworkController framework;

	@Before
	public void setup() {
		framework = new JavaScriptFrameworkController(repository);
	}

	@Test
	public void getFramework() throws Exception {
		MvcResult result = this.mvc.perform(get("/frameworks/list"))
				.andExpect(status().isOk())
				.andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println("get result :   " + content);
	}

	@Test
	public void addFramework() throws Exception {
		String firstFramework = produceFrameworkJson("esqo", "1.0.0");
		String secondFramework = produceFrameworkJson("", "2.0.0");
		String thirdFramework = produceFrameworkJson("esqo2", "");
		MvcResult firstResult = this.mvc
				.perform(put(createPath).content(firstFramework).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("OK"))
				.andExpect(jsonPath("$.id").value("1")).andReturn();

		String content = firstResult.getResponse().getContentAsString();
	//	System.out.println(content);

		MvcResult secondResult = this.mvc
				.perform(put(createPath).content(secondFramework).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("ERROR")).andReturn();

		content = secondResult.getResponse().getContentAsString();
	// 	System.out.println(content);

		MvcResult thirdResult = this.mvc
				.perform(put(createPath).content(thirdFramework).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("ERROR")).andReturn();

		content = thirdResult.getResponse().getContentAsString();
		
		String awesomeFramework = produceFrameworkJson("toBEDeleted", "9.9.9");
		this.mvc.perform(put(createPath).content(awesomeFramework).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("OK"))
				.andExpect(jsonPath("$.id").value("2")).andReturn();
		// System.out.println(content);
	}
	
	@Test
	public void updateFramework() throws Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		framework.setId(1L);
		framework.setName("esqoLLL");
		String frameworkJson = frameworkToJson(framework);
		String content = "";
		MvcResult result = this.mvc
				.perform(patch(updatePath).content(frameworkJson).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("OK"))
				.andExpect(jsonPath("$.id").value("1")).andReturn();
		
		content = result.getResponse().getContentAsString();
		System.out.println("update result : " + content);
		
		this.mvc.perform(patch(updatePath).content(frameworkJson).contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value("ERROR"));
		
		result = this.mvc.perform(get("/frameworks/list"))
				.andExpect(status().isOk())
				.andReturn();
		content = result.getResponse().getContentAsString();
		System.out.println("get result after update :   " + content);
		
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		framework.setDeprecationDate(date);
		frameworkJson = frameworkToJson(framework);
		result = this.mvc
				.perform(patch(updatePath).content(frameworkJson).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("OK"))
				.andExpect(jsonPath("$.id").value("1")).andReturn();
		
		result = this.mvc.perform(get("/frameworks/list"))
				.andExpect(status().isOk())
				.andReturn();
		content = result.getResponse().getContentAsString();
		System.out.println("get result after update1 :   " + content);
		
		now = System.currentTimeMillis();
		date = new Date(now);
		
		framework.setDeprecationDate(date);
		frameworkJson = frameworkToJson(framework);
		result = this.mvc
				.perform(patch(updatePath).content(frameworkJson).contentType("application/json"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.status").value("OK"))
				.andExpect(jsonPath("$.id").value("1")).andReturn();
		
		result = this.mvc.perform(get("/frameworks/list"))
				.andExpect(status().isOk())
				.andReturn();
		content = result.getResponse().getContentAsString();
		System.out.println("get result after update2 :   " + content);
	}
	
	@Test
	public void deleteFramework() throws Exception {
		String validId = "2";
		String invalidId = "44";
		MvcResult result = this.mvc
				.perform(delete(deletePath + validId).contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value("OK"))
				.andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println("deleted content : " +  content);
		
		this.mvc.perform(delete(deletePath + validId).contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value("ERROR"))
				.andReturn();
		
		this.mvc.perform(delete(deletePath + invalidId).contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value("ERROR"))
				.andReturn();
	}
	
	@Test
	public void getFrameworkById() throws Exception {
		MvcResult result = this.mvc.perform(get(getByIdPath + "1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("esqo"))
				.andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println("get by id result :   " + content);
	}
	
	@Test
	public void getFrameworkByName() throws Exception {
		MvcResult result = this.mvc.perform(get("/frameworks/get/byname/esqo"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value("1"))
				.andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println("get by name result :   " + content);
	}

	private String produceFrameworkJson(String name, String version) {
		JavaScriptFramework framework = createDummyFramework(name, version);
		return frameworkToJson(framework);
	}

	private JavaScriptFramework createDummyFramework(String name, String version) {
		List<String> versions = null;
		if (!version.equals("")) {
			versions = new ArrayList<>();
			versions.add(version);
		}
		JavaScriptFramework framework = new JavaScriptFramework(name, versions);
		framework.setHypeLevel(9.9f);
		return framework;
	}

	private String frameworkToJson(JavaScriptFramework framework) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String json = mapper.writeValueAsString(framework);
			return json;
		} catch (Exception e) {
			return "";
		}
	}

}
